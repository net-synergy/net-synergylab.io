;;; .dirs-locals.el --- Set up automatic time-stamping on save -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 David R. Connell
;;
;; Maintainer: David R. Connell <davidconnell12@gmail.com>
;; Created: January 20, 2023
;; Modified: January 20, 2023
;; Version: 0.0.1
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;; The following code updates the last modified date for org-mode files with
;; the current day on save. This allows for automatically keeping track of
;; modification on the published hugo site.
;;; Code:

((org-mode . ((time-stamp-pattern . "#\\+date:[ \t]+<%:y-%02m-%02d %3a>")
	      (eval . (add-hook 'before-save-hook #'time-stamp nil t))
	      ;; (eval . (auto-insert-mode))
	      (auto-insert-alist . '(("\\.org" . "Org header")
				     "Basic"
				     "#+title: " _ \n
				     "#+date: <>" \n
				     "#+weight: " \n)))))

(provide '.dir-locals)
;;; .dirs-locals.el ends here
