#+title: Abstract2Gene
#+date: <2023-02-02 Thu>
#+weight: 20

- See the ~abstract2gene~ section of the presentation on the side menu.

Goal: to connect social science publications to AD research.

We are using natural language processing methods to identify common words associated with genes.
Using genetics publications to train a model then passing the model abstracts from non-genetics publications to identify genes related to their work.

Main challenges are finding a way to only consider words that would be used outside of genetics research.
So far the model has been highly accurate when testing and training on genetic publications but it relies on words related to genetic vocabulary which would not exist in social science papers.
Ideally, the model should be picking up on descriptions of high level symptoms.

Steps:
- Add publication--gene edges (DONE)
- Create abstracts' features
- Train/test model based on these features
- Validate

* Create feature set
The features require being able to remove words that would not be found social science publications and "normalizing" words.
Removing these words will likely be their own project.
The current solution is creating a bank of social science words by scanning through social science abstracts then removing any words not in that bank from the genetic abstracts before sending them to the model.
To do this we first need to identify social science publications.
A simple solution would be to find a list of social science journals online then assume any article from one of those journals is a social science article.
This will likely be enough for now but ideally we will be able to extend this by using labeled articles to label other unlabeled articles (those from journals without a clear label) based on the references and authors.
This method will also likely be useful for propagating keywords across the network (such as from MeSH terms from pubmed that are not present on all articles).

Once the articles can be labeled as social science we can go back and look at the rest of the feature set generation procedure currently used by [[https://gitlab.com/net-synergy/abstract2gene][abstract2gene]].
* Design model
In the first feasibility analyses of ~abstract2gene~ I used a simple binary model.
This predicts whether a specific gene is or is not related to the provided article's feature set.
For the full model, instead of testing a specific gene, it should return all genes that it thinks is related.
* Validate
Validation will be difficult because there will be no way to have gene labels for social science publications and genetic publications cannot be used in there place since there may be differences in the style of the publications and the meaning of the words authors from different fields use.
For initial validation, using genetic publications in the test set is still useful for ensuring the model is not completely off but another method for validation will be needed to determine if the results from genetic publications does translate to abstracts from other fields.
