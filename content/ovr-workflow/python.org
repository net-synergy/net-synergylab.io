#+title: Python projects
#+date: <2023-02-04 Sat>
#+weight: 20

Most of the work we are doing will likely be written python code.
Generally, the end user should be working with python packages rather than lower-level code.
Here's a brief description of how my python packages are set up and how to get started with them.

* Setting up environment
Package management is important for ensuring packages work the same on multiple computers.
This is particularly true when using unstable packages (i.e. the ones we're writing [[{{< relref "collaborating.org#versioning">}}][see versioning]]) so it's necessary to keep track of dependency versions.
Unfortunately there are some flaws in the way python handles packages which makes it impossible to ensure all dependencies' requirements are satisfied in all cases due to the age of python (only a single version of dependency can exist at runtime, which means if two packages need a common dependency they must compromise on a version, this also means potentially indeterminate dependency resolution)[fn:1].
Despite it's issues, modern tools can help do a pretty good job getting a package running consistently.

The easiest method for running any of these packages is using [[https://python-poetry.org/docs/][poetry]].
I have my projects set up so that they will produce a ~pyproject.toml~ listing the packages dependencies, that will work with ~poetry~.
I suggest you use this over manually installing with ~pip~ as it creates a virtual environment, which means the installed packages will be local to the project, so multiple projects on your computer can use different versions of the same dependencies preventing "dependency hell" where updating one projects dependencies breaks your other projects.
To use, after installing poetry (you can install with ~pip~), run ~poetry shell~ to drop into a new shell with the private environment set up, then run ~poetry install~ to get the needed packages.
While in this shell, you can now run python normally.
See [[https://python-poetry.org/docs/basic-usage/#activating-the-virtual-environment][Activating the virtual environment]] for more information.

* Writing tests
For python packages, I use the ~pytest~  testing framework.
This is a simple testing framework but also has many plugins to allow it to handle most all needs.
The required testing packages should be in the project's ~pyproject.toml~, under the ~[tool.poetry.dev-dependencies]~ heading.
This allows you to run tests with ~poetry run pytest~ and will ensure any extra plugins are installed.
See the [[https://docs.pytest.org/en/7.1.x/contents.html][pytest docs]] for more options such as filtering tests run and writing tests.
I would also suggest taking a look at preexisting tests in the projects as well as in other python packages such as ~pandas~ and ~scipy~ to learn how to write tests.

* Formatting code
Code formatters keep source files consistent.
However, if everyone chooses their own formatter, git will pickup irrelevant changes to whitespace that will obscure the diffs.
For all my projects, I use the black formatter.
I like it because it's goal is to be as strict as possible so that the same code will always be formatted the same way, so that the developer has little control over the formatting.
Your text editor will almost certainly have an extension for formatters, or specifically [[https://github.com/psf/black][black]], this will allow it to automatically run the formatter when saving the file.

* Documenting API
All public methods/functions should be documented.
Specifically the should have docstrings describing how to call them and what their arguments do.
Don't skip this.

* Nix for the interested
As a substitute for ~poetry~, [[https://nixos.org/][nix]] is a general purpose package manager.
Because nix is not limited to python, it can be used to create private environments for multiple language projects without needing multiple tools and can be used for all your projects so you don't have to use a new tool for each language.
It is much more complicated to get started with than poetry, but if interested, it will likely be beneficial to you in the long run.

You'll notice that my packages have ~flake.nix~ and ~flake.lock~ files, these tell nix how to build a package and it's development environment in a (essentially) perfectly reproducible manner.
(Nix will fully construct a packages dependency graph ensuring that it's dependency are built with the right version of their dependencies and so on, however, this still can't fix the flaws in python package management, hence the not quite perfectly reproducible in all cases.)
By using a flake, the environment can be created with:

#+begin_src bash
  nix develop "flake-url#package"
#+end_src

Where the flake url can be a git host or a local package.
Personally, I use this with ~direnv~ which integrates into my text editor.
With this set up my system environment is changed when I move between packages, meaning I can open one python package, run python, then go to another package and run python again and both instances will have access to different python packages.

[fn:1] Newer languages like Julia have been designed early on with package managers that pin the state of it's dependencies and allow for multiple versions of the same package at runtime.
It uses method similar to the Nix package manager.
