#+title: Collaborating
#+date: <2023-02-04 Sat>
#+weight: 10

All collaboration should be done by making merge requests to an "upstream" repo.
This requires making a "fork" of the upstream repo, making your changes there, then making a merge request to pull your changes into the upstream repo.

* Merge requests
Generally, if you work on an open-source project you'll be expected to fork and then make a merge request as opposed to working directly on the original repo.
This prevents the author from having to give many people access to their project and prevents someone from adding changes the author doesn't like.

** Fork
:PROPERTIES:
:header-args: :results code :session *bash*
:END:
To fork, make an account on the host where the project you want to work on is (in our case gitlab).
Then go to the project, and click the fork button in the top right hand of the screen.
This will produce an exact copy of the repo under your account.
It's a lot like cloning a repo except that it keeps some extra (host specific) links to the forked repo in order to allow for merge requests.

You will now make a clone of the fork on your machine (see the clone button, semi-top right).
At this point, there are three repos of interest to you: the original repo on gitlab, your fork on gitlab, and your local clone.
We'll call your fork "origin" as that's the traditional name for your hosted repo.
The original will then be "upstream" and your local doesn't get a special name.

For example using the ~pubnet~ repo:
#+begin_src bash :results none
  cd ~/clones # Wherever you want to store it
  git clone git@gitlab.com:DavidRConnell/pubnet.git
  cd pubnet
#+end_src

{{% notice style="note" %}}
When cloning a project you'll be pushing to it's convenient to use ssh (the url that looks like ~git@gitlab.com:repo/path.git~) instead of https because an ssh key can verify your identity which means not having to enter your username and password when pushing to confirm you have write access.
This is also technically more secure than entering a password, but if you have a strong password that shouldn't be a concern.
This requires setting up ssh keys though.
See [[https://docs.gitlab.com/ee/user/ssh.html][Use SSH keys to communicate with GitLab | GitLab]] for more information.
It's a good thing to know about if you're not familiar.
{{% /notice %}}

Once we've cloned we can check it's remote repositories.

#+begin_src bash
  git remote show
#+end_src

#+RESULTS:
#+begin_src bash
origin
#+end_src

And see where that repo is located:

#+begin_src bash
  git remote get-url origin
#+end_src

#+RESULTS:
#+begin_src bash
git@gitlab.com:DavidRConnell/pubnet.git
#+end_src

By default it makes the origin remote for you based on where it was cloned from.
To keep track of changes upstream, you'll also want an upstream remote:

#+begin_src bash
  git remote add upstream https://gitlab.com/net-synergy/pubnet.git
  git remote show
#+end_src

#+RESULTS:
#+begin_src bash
origin
upstream
#+end_src

The https scheme was used here since the repo is public (so we can pull without special access) and we won't be pushing to it directly.
To use ssh with the upstream, the upstream maintainer would have to explicitly add your public ssh-key to the project.

** Branching
To work on a feature, create a new feature branch.
For example to add benchmarking for issue [[https://gitlab.com/net-synergy/pubnet/-/issues/4][#4]]:

#+begin_src bash :results none
  git switch -c feature/add-benchmarks
#+end_src

The name should be a clear summary so in a pull request and in the history it will be obvious what the group of commits are for.
(Additionally, if you don't branch and make a pull request using your ~master~ branch, then I'll have to create a name myself to check it out locally because I already have a ~master~ branch and I'd rather you come up with the name.)

Besides clarity, working off branches will help if multiple people are working on the same project.
If you fork the project, then someone else adds changes upstream, you'll histories will have diverged and it will be hard to re-sync them:

{{< mermaid align="center" >}}
%%{init:{
"themeVariables":{"commitLabelFontSize":"18px"},
"gitGraph":{"mainBranchName":"upstream/master"}
}}%%
gitGraph
    commit id: "a"
    commit id: "b"
    branch coworker/feature-a
    checkout coworker/feature-a
    commit id: "c"
    commit id: "d"
    checkout upstream/master
    branch origin/master
    checkout origin/master
    commit id: "e"
    commit id: "f"
    checkout upstream/master
    merge coworker/feature-a tag: "Merge feature-a"
{{< /mermaid >}}

In the above scenario, we have branches across three repos: the upstream, yours (origin), and a coworker's.
Both you and a coworker have "based" work off of commit ~b~.
Coworker's work is merged into upstream.
Now, it can be difficult to merge your work with upstream because the new changes could conflict with your changes, this would require performing a merge between upstream and your branch.
(Even if you're changes don't conflict, this is still a challenging spot to be in.)
And if that happens, then when your commits are merged into upstream, there's an extra merge commit that is specific to your branch in the upstream branch.

{{< mermaid align="center" >}}
%%{init:{
"themeVariables":{"commitLabelFontSize":"18px"},
"gitGraph":{"mainBranchName":"upstream/master"}
}}%%
gitGraph
    commit id: "a"
    commit id: "b"
    branch coworker/feature-a
    checkout coworker/feature-a
    commit id: "c"
    commit id: "d"
    checkout upstream/master
    branch origin/master
    checkout origin/master
    commit id: "e"
    commit id: "f"
    checkout upstream/master
    merge coworker/feature-a tag: "Merge feature-a"
    checkout origin/master
    merge upstream/master tag: "Merge master"
    checkout upstream/master
    merge origin/master tag: "Merge master"
{{< /mermaid >}}

As can be seen above, for a couple of reasons this is confusing and ugly.
Note that it would not be possible to do a fast-forward here since the history between upstream/master and origin/master is not a single linear path.
There is a way to get out of this using rebasing, but it's much easier if you use a feature branch.
In the same scenario if you used a feature branch:

*Upstream*
{{< mermaid align="center" >}}
%%{init:{
"themeVariables":{"commitLabelFontSize":"18px"},
"gitGraph":{"mainBranchName":"master"}
}}%%
gitGraph
    commit id: "a"
    commit id: "b"
    branch feature-a
    checkout feature-a
    commit id: "c"
    commit id: "d"
    checkout master
    merge feature-a tag: "Merge feature-a"
{{< /mermaid >}}

*Origin*
{{< mermaid align="center" >}}
%%{init:{
"themeVariables":{"commitLabelFontSize":"18px"},
"gitGraph":{"mainBranchName":"master"}
}}%%
gitGraph
    commit id: "a"
    commit id: "b"
    branch feature-b
    checkout feature-b
    commit id: "e"
    commit id: "f"
    checkout master
{{< /mermaid >}}

Where upstream and origin's repos are now shown separated.
Here, before a merge request, you'd pull from upstream:

#+begin_src bash :eval no
  git checkout master
  git pull
#+end_src

*Origin*
{{< mermaid align="center" >}}
%%{init:{
"themeVariables":{"commitLabelFontSize":"18px"},
"gitGraph":{"mainBranchName":"master"}
}}%%
gitGraph
    commit id: "a"
    commit id: "b"
    branch feature-a
    branch feature-b
    checkout feature-a
    commit id: "c"
    commit id: "d"
    checkout master
    merge feature-a tag: "Merge feature-a"
    checkout feature-b
    commit id: "e"
    commit id: "f"
    checkout master
{{< /mermaid >}}

The feature branch helps because your ~master~ branch is now unchanged, meaning you'll always be able to pull from ~upstream/master~ to ~origin/master~ without any issues.
Once you have a local copy of the changes to the history, it's much easier to manage histories and prevent unneeded merge commits.
It's possible to do this with the "fetch head" instead of using a feature branch, but it's less work to branch beforehand.

Now that your ~master~ is in sync with ~upstream/master~, "rebase" your feature branch onto local ~master~.
As mentioned before, both ~feature-a~ and ~feature-b~ are based off of commit ~b~.
The ~rebase~ command allows you to change the commit your changes are based off.
Git checks what has changed between commit ~f~ and ~b~, then applies those changes to another commit.

#+begin_src bash :eval no
  git checkout feature-b
  git rebase master
#+end_src

*Origin*
{{< mermaid align="center" >}}
%%{init:{
"themeVariables":{"commitLabelFontSize":"18px"},
"gitGraph":{"mainBranchName":"master"}
}}%%
gitGraph
    commit id: "a"
    commit id: "b"
    branch feature-a
    checkout feature-a
    commit id: "c"
    commit id: "d"
    checkout master
    merge feature-a tag: "Merge feature-a"
    branch feature-b
    checkout feature-b
    commit id: "e"
    commit id: "f"
    checkout master
{{< /mermaid >}}

Your feature branch is now directly in front of ~upstream/master~ and ready for a merge request.

** Proposing merge requests
Once you're changes are ready, or if you're just looking for feedback, you can submit a merge request.
To do this, go to your fork on gitlab.
The sidebar on the left will have a "merge requests" button, click it and there will be a button for "New merge request."
** Updating merge requests
Merge requests are not static.
If you make a merge request then push new changes to the same branch of your repo, they'll show up in the merge request.
Because of this you can submit a merge request before the code is finalized if you want to show someone your current work.
Once a merge request is made, anyone can then look at the changes, work with them on their machine, discuss the changes, and I'll even be able to make commits directly to your branch.
* Versioning
:PROPERTIES:
:CUSTOM_ID: versioning
:END:
This is not something that you'll have to worry about, but I use [[https://semver.org/][Semantic Versioning]] for my packages.
This is a formalization of what people tend to use.
Under this method, packages are version as ~major.minor.patch~.
Patch is incremented whenever fixes or optimizations are applied that should not impact how the end user uses the package.
Minor is for additions to the API but these changes do not break the current API.
Then major is for API breaking changes.
Before the package has hit major version 1, these rules are not strict, as it is expected that the package will have frequent breaking changes early in development (as a consequence, version ~1.0.0~ is the first stable release).
This is useful to allow dependent packages to lock onto major versions for its dependency.
Dependencies can then be upgraded freely without breaking the dependent package.
And, when the developer is ready, the dependent package can be upgraded to use a newer major version of package.
* See also
See the github and gitlab docs as references for working with git forges. Many topics are interchangeable between the two providers. Specifically for more information on Pull/Merge requests see:
- [[https://docs.gitlab.com/ee/user/project/merge_requests/][Merge requests | GitLab]]
- [[https://docs.github.com/en/pull-requests][Pull requests - GitHub Docs]]
